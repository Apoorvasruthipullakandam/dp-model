package com.scholastic.dp.api.model.validation;

import lombok.Data;


@Data
public class RecordError {
	private String code;
	private String message;
}
