package com.scholastic.dp.api.model.subscription;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.foundation.core.patch.PatchMerge;
import com.scholastic.foundation.core.patch.PatchMerge.PatchStrategy;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author kyrylo.torbin
 * @version 1.0 11/6/2015
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@PatchMerge(strategy = PatchStrategy.MERGE)
@Data
public class AccessCode {

    @NotEmpty
    private String code;

    @NotEmpty
    private Integer subscriptionId;

    @NotEmpty
    private Integer maxUses;

    @NotEmpty
    private Integer remainingUses;
}