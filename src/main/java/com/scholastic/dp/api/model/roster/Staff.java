package com.scholastic.dp.api.model.roster;

import java.io.Serializable;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.dp.api.model.user.UserIdentifiers;
import com.scholastic.foundation.core.patch.PatchMerge;
import com.scholastic.foundation.core.patch.PatchMerge.PatchStrategy;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

@JsonIgnoreProperties(ignoreUnknown = true)
@PatchMerge(strategy = PatchStrategy.MERGE)
@Data
@EqualsAndHashCode(callSuper=false)
@SuppressWarnings("serial")
public class Staff  implements Serializable {
    public static final int MAX_LEN_FIRST_NAME = 30;
    public static final int MAX_LEN_LAST_NAME = 30;
	public static final int MAX_LEN_EMAIL = 256;

	private String id;

    @NonNull
    private UserIdentifiers identifiers = new UserIdentifiers();
    
    @NotEmpty
    @Length(max = MAX_LEN_FIRST_NAME)
    private String firstName;
    
    @NotEmpty
    @Length(max = MAX_LEN_LAST_NAME)
    private String lastName;
    
    @Email
    @Length(max = MAX_LEN_EMAIL)
    private String email;
}
