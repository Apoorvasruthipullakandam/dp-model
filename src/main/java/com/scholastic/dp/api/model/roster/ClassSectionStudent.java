package com.scholastic.dp.api.model.roster;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.foundation.core.patch.PatchMerge;
import com.scholastic.foundation.core.patch.PatchMerge.PatchStrategy;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown=true)
@PatchMerge(strategy=PatchStrategy.MERGE)
@Data
public class ClassSectionStudent  {
	
    private String id;
   
    private Integer classSectionId;
	
    private Integer studentId;
}
