package com.scholastic.dp.api.model.product;

import lombok.Data;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.foundation.core.patch.PatchMerge;
import com.scholastic.foundation.core.patch.PatchMerge.PatchStrategy;

@JsonIgnoreProperties(ignoreUnknown = true)
@PatchMerge(strategy = PatchStrategy.MERGE)
@Data
public class SubscriptionProduct {

    public static final int MAX_LEN_NAME = 30;

    private String id;

    @NotEmpty
    @Length(max = MAX_LEN_NAME)
    private String name;

    @NotEmpty
    private String applicationId;

    private Boolean active;

}
