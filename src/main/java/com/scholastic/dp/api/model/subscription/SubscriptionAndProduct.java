package com.scholastic.dp.api.model.subscription;

import com.scholastic.dp.api.model.product.Application;
import com.scholastic.dp.api.model.product.SubscriptionProduct;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubscriptionAndProduct {
    private Subscription subscription;
    private SubscriptionProduct product;
    private Application application;
}
