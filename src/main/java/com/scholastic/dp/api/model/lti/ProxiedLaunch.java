package com.scholastic.dp.api.model.lti;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.foundation.core.patch.PatchMerge;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@PatchMerge(strategy = PatchMerge.PatchStrategy.MERGE)
@Data
public class ProxiedLaunch implements Serializable {
    private static final long serialVersionUID = 1L;
    private Map<String, String> params;
    private String launchUrl;

    public Map<String, String> getParams() {
        return params;
    }

    public String getLaunchUrl() {
        return launchUrl;
    }

    public ProxiedLaunch(Map<String, String> params, String launchUrl) {
        super();
        this.params = params;
        this.launchUrl = launchUrl;
    }

    public ProxiedLaunch() {}
}