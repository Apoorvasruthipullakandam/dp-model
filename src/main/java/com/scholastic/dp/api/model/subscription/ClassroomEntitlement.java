package com.scholastic.dp.api.model.subscription;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.dp.api.model.roster.Student;
import com.scholastic.foundation.core.patch.PatchMerge;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * @author kyrylo.torbin
 * @version 1.0 11/20/2015
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@PatchMerge(strategy = PatchMerge.PatchStrategy.MERGE)
@Data
public class ClassroomEntitlement {

    @NotNull
    private List<SubscriptionAndProduct> subscriptions;

    @NotNull
    private Map<String, String[]> entitlements;

    @NotEmpty
    private List<Student> students;
}