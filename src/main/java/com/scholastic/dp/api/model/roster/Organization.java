package com.scholastic.dp.api.model.roster;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import lombok.Data;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.foundation.core.patch.PatchMerge;
import com.scholastic.foundation.core.patch.PatchMerge.PatchStrategy;

@JsonIgnoreProperties(ignoreUnknown = true)
@PatchMerge(strategy = PatchStrategy.MERGE)
@Data
public class Organization {

    public static final int MAX_LEN_NAME = 100;
    private String id;

    @NotEmpty
    @Length(max = MAX_LEN_NAME)
    private String name;

    private String parent;

    @NotEmpty
    @Pattern(regexp = "school|state|national|local")
    private String orgType;

    @Valid
    private OrgIdentifiers identifiers;
    
    public void setName(String name) {
    	this.name = name == null ? null : name.trim();
    }    
}
