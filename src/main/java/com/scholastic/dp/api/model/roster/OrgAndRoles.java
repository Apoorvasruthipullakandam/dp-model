package com.scholastic.dp.api.model.roster;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrgAndRoles {
    private Organization org;
    private List<String> roles;
}
