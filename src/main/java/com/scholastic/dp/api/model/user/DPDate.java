package com.scholastic.dp.api.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class DPDate {
	private Integer dayOfMonth;
	private Integer monthOfYear;
	private Integer year;
	
	public DPDate(){}
	
	public DPDate(int month, int dayOfMonth, int year) {
		setMonthOfYear(month);
		setDayOfMonth(dayOfMonth);
		setYear(year);
	}
}
