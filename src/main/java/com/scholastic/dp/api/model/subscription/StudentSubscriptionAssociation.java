package com.scholastic.dp.api.model.subscription;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.foundation.core.patch.PatchMerge;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author kyrylo.torbin
 * @version 1.0 11/18/2015
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@PatchMerge(strategy = PatchMerge.PatchStrategy.MERGE)
@Data
public class StudentSubscriptionAssociation {

    @NotEmpty
    private String studentId;

    @NotEmpty
    private String subscriptionId;
}