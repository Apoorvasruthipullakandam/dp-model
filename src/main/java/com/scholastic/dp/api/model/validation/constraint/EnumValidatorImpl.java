package com.scholastic.dp.api.model.validation.constraint;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EnumValidatorImpl implements ConstraintValidator<EnumValidator,String> {
    private List<String> allowedValues = null;
    private boolean required;
    private boolean caseSensitive;

    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null ) {            
            return !required;
        }
        if( !caseSensitive ) {
            value = value.toUpperCase();
        }
        return allowedValues.contains(value);
    }


    public void initialize(EnumValidator constraintAnnotation) {

        Class<? extends Enum<?>> enumClass = constraintAnnotation.enumType();
        @SuppressWarnings("rawtypes")
        Enum[] enumValArr = enumClass.getEnumConstants();

        this.required = constraintAnnotation.required();
        this.caseSensitive = constraintAnnotation.caseSentitive();
        
        this.allowedValues = new ArrayList<String>();
        for (@SuppressWarnings("rawtypes")
        Enum enumVal : enumValArr) {
            String value = enumVal.toString();
            if( !caseSensitive ) {
                value = value.toUpperCase();
            }                    
            allowedValues.add(value);
        }
    }
    
    public boolean isRequired() {
        return required;
    }
}
