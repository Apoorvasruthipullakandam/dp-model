package com.scholastic.dp.api.model.subscription;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.foundation.core.patch.PatchMerge;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author kyrylo.torbin
 * @version 11/12/2015
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@PatchMerge(strategy = PatchMerge.PatchStrategy.MERGE)
@Data
public class TeacherSubscriptionAssociation {

    @NotEmpty
    private String staffId;

    @NotEmpty
    private String subscriptionId;
}