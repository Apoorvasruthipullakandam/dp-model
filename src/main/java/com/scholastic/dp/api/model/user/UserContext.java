package com.scholastic.dp.api.model.user;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.dp.api.model.roster.Staff;
import com.scholastic.dp.api.model.roster.Student;
import com.scholastic.foundation.core.patch.PatchMerge;
import com.scholastic.foundation.core.patch.PatchMerge.PatchStrategy;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
@PatchMerge(strategy=PatchStrategy.MERGE)
@SuppressWarnings("serial")
public class UserContext implements Serializable {	
    private UserIdentifiers userIdentifiers;
    private Student student;
    private Staff staff;

    public static UserContext forStaff(Staff staff) {
    	UserContext context = new UserContext();
    	context.setStaff(staff);
    	context.setUserIdentifiers(staff.getIdentifiers());
    	return context;
    }
    
    public static UserContext forStudent(Student student) {
    	UserContext context = new UserContext();
    	context.setStudent(student);
    	UserIdentifiers identifiers = new UserIdentifiers();
    	identifiers.setStudentId(student.getId());
    	context.setUserIdentifiers(identifiers);
    	return context;
    }
}
