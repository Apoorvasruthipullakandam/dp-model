package com.scholastic.dp.api.model.roster;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.foundation.core.patch.PatchMerge;
import com.scholastic.foundation.core.patch.PatchMerge.PatchStrategy;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@PatchMerge(strategy = PatchStrategy.MERGE)
@Data
public class ClassSectionStaff  {
	@NotEmpty
	private String primaryTeacherId;

	public ClassSectionStaff() {		
	}
	
	public ClassSectionStaff(String primaryTeacherId) {
		this.primaryTeacherId = primaryTeacherId;
	}
}