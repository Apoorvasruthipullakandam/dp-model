package com.scholastic.dp.api.model.validation;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class SimpleFieldError extends RecordError {
    private String field;
    private String objectName;
}
