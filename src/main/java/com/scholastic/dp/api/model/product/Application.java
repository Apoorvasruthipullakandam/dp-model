package com.scholastic.dp.api.model.product;



import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.foundation.core.patch.PatchMerge;
import com.scholastic.foundation.core.patch.PatchMerge.PatchStrategy;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@PatchMerge(strategy = PatchStrategy.MERGE)
@Data
public class Application {
	
	 public static final int NAME_MAX_LEN = 30;
	 public static final int URL_MAX_LEN = 1000;
	 public static final int DESCRIPTION_MAX_LEN = 2000;
	
	private String id;
	
	@Length(max=NAME_MAX_LEN)
	@NotEmpty
	private String name;
	
	@Length(max=DESCRIPTION_MAX_LEN)
	private String description;
	
	@Length(max=URL_MAX_LEN)
	private String url;
	
	@Length(max=URL_MAX_LEN)
	private String thumbnail;
	
	private Boolean active;

}
