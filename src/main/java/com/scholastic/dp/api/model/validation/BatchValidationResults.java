package com.scholastic.dp.api.model.validation;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown=true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BatchValidationResults<T>  {
    private List<T> records;
    private List<BatchValidationRecord> violations;   
}
