package com.scholastic.dp.api.model.roster;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.foundation.core.patch.PatchMerge;
import com.scholastic.foundation.core.patch.PatchMerge.PatchStrategy;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@PatchMerge(strategy = PatchStrategy.MERGE)
@Data
public class ClassSection {

	public static final int MAX_LEN_NICKNAME = 30;

	private String id;

	@NotEmpty
	private String organizationId;

	@Valid
	private ClassSectionStaff staff;

	@NotEmpty
	@Length(max = MAX_LEN_NICKNAME)
	private String nickname;

	@NotEmpty
	private List<String> grades;

	@Valid
	private StudentAuthenticationMethod studentAuthenticationMethod;
}
