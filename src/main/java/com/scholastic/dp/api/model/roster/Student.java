package com.scholastic.dp.api.model.roster;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.dp.api.model.user.DPDate;
import com.scholastic.dp.api.model.user.UserCredentials;
import com.scholastic.dp.api.model.validation.constraint.EnumValidator;
import com.scholastic.foundation.core.patch.PatchMerge;
import com.scholastic.foundation.core.patch.PatchMerge.PatchStrategy;

import lombok.Data;
import lombok.EqualsAndHashCode;

@JsonIgnoreProperties(ignoreUnknown=true)
@PatchMerge(strategy=PatchStrategy.MERGE)
@Data
@EqualsAndHashCode(callSuper=false)
@SuppressWarnings("serial")
public class Student implements Serializable {
	public static final int MAX_LEN_FIRST_NAME = 30;
	public static final int MAX_LEN_LAST_NAME = 30;
	
	// Names can consist of one or more words separated by spaces or dashes
	// e.g. John-John and Mary Sue are both valid but M@ry or M
	private static final String PATTERN_NAME = "[a-zA-Z]+([ -][a-zA-Z]+)*";
	
	private String id;

	@NotEmpty
    @Length(max=MAX_LEN_FIRST_NAME)
    @Pattern(regexp=PATTERN_NAME)
    private String firstName;

    @NotEmpty
    @Length(max=MAX_LEN_LAST_NAME)
    @Pattern(regexp=PATTERN_NAME)
    private String lastName;
	
    private Boolean active ;
       
    private String gender;
      
    private DPDate dob;
    
    @NotEmpty
    // Use case insensitive since enum is upper case and we set to lowercase 
    @EnumValidator(enumType=Grade.class, message="error.invalid.grade", caseSentitive=false) 
    private String grade;
   
    private String studentId;
    
    @Valid
    private UserCredentials credentials;   

    public void setGrade(String sGrade) {
        // Retrieve the canonical form of the grade (e.g. "3" --> "third")
        Grade gradeEnumVal = Grade.getGrade(sGrade);
        if( gradeEnumVal != null ) {
            // The grade is valid. Use the canonical form
            this.grade = gradeEnumVal.getCode();
        }
        else {
            // The grade is invalid, so leave as is. Will be flagged by the validator
            this.grade = sGrade;
        }
    }
}