package com.scholastic.dp.api.model.roster;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Grade {
    PREK("Pre-K", -1),
    KINDERGARTEN("Kindergarten", 0),    
    FIRST("1st Grade", 1),
    SECOND("2nd Grade", 2),
    THIRD("3rd Grade", 3),
    FOURTH("4th Grade", 4),
    FIFTH("5th Grade", 5),    
    SIXTH("6th Grade", 6),
    SEVENTH("7th Grade", 7),
    EIGHTH("8th Grade", 8),
    NINTH("9th Grade", 9),
    TENTH("10th Grade", 10),
    ELEVENTH("11th Grade", 11),
    TWELFTH("12th Grade", 12);
    
    private String label;
    private int numericValue;
    
    public String getCode() {
        return name().toLowerCase();
    }
    
    private static Map<String, Grade> MAP = new HashMap<String, Grade>();
    
    static {
        for(Grade grade : Grade.class.getEnumConstants() ) {
            MAP.put(grade.name().toLowerCase(), grade);
            MAP.put(grade.label.toLowerCase(), grade);
            MAP.put(Integer.toString(grade.numericValue), grade);
        }
    }
    
    public static Grade getGrade(String code ) {
        if( code == null ) {
            return null;
        }
        return MAP.get(code.toLowerCase().trim());
        
    }
}
