package com.scholastic.dp.api.model.user;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.foundation.core.patch.PatchMerge;
import com.scholastic.foundation.core.patch.PatchMerge.PatchStrategy;

import lombok.Data;

@Data
@PatchMerge(strategy = PatchStrategy.MERGE)
@JsonIgnoreProperties(ignoreUnknown=true)
@SuppressWarnings("serial")
public class UserIdentifiers implements Serializable {    
    private String iamUserId;
    private String staffId;
    private String studentId;
}
