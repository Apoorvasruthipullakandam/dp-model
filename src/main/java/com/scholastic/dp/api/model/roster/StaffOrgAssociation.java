package com.scholastic.dp.api.model.roster;

import java.io.Serializable;
import java.util.List;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.dp.api.model.validation.constraint.EachEnum;
import com.scholastic.foundation.core.patch.PatchMerge;
import com.scholastic.foundation.core.patch.PatchMerge.PatchStrategy;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@PatchMerge(strategy = PatchStrategy.MERGE)
@Data
@SuppressWarnings("serial")
public class StaffOrgAssociation implements Serializable {

    public static final int MAX_LEN = 30;
    private String id;

    @NotEmpty
    @Length(max = MAX_LEN)
    private String staffId;

    @NotEmpty
    @Length(max = MAX_LEN)
    private String orgId;

    @NotEmpty
    @EachEnum(enumType=OrgRole.class,caseSentitive=false)
    private List<String> orgRoles;
    
    private Boolean active;
    
    public void setOrgRoles(List<String> roles) {
        if( roles != null ) {
            for(String role : roles) {
                try {
                    role = OrgRole.getOrgRole(role).getCode();
                }
                catch(Exception e) {                    
                }
            }
        }
        this.orgRoles = roles;
    }
}
