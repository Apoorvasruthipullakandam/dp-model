package com.scholastic.dp.api.model.roster;


import java.io.Serializable;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.foundation.core.patch.PatchMerge;
import com.scholastic.foundation.core.patch.PatchMerge.PatchStrategy;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@PatchMerge(strategy = PatchStrategy.MERGE)
@Data
@SuppressWarnings("serial")
public class StudentAuthenticationMethod implements Serializable {

	public static final int MAX_LEN = 30;

	@NotEmpty
	@Length(max = MAX_LEN)
	private String type;

	private Boolean enabled;

	private Boolean requirePassword;
}