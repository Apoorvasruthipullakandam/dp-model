package com.scholastic.dp.api.model.subscription;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.scholastic.dp.api.model.validation.constraint.EnumValidator;
import com.scholastic.foundation.core.patch.PatchMerge;
import com.scholastic.foundation.core.patch.PatchMerge.PatchStrategy;

import lombok.Data;
@PatchMerge(strategy = PatchStrategy.MERGE)
@Data
public class License {
    
    public static final int MAX_LEN_NAME = 30;
    
    @NotEmpty
    @EnumValidator(enumType=LicenseType.class, caseSentitive=false)
    String type;
    
    @NotNull
    @Min(value=1)
    Integer quantity;
    
    @Min(value=0)
    Integer remaining;
}
