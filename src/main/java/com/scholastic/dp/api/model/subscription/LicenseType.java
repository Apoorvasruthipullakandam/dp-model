package com.scholastic.dp.api.model.subscription;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum LicenseType {
    TEACHER, STUDENT;
    
    public String getCode() {
        return name().toLowerCase();
    }
    
    public LicenseType getLicenseType(String code) {
        return LicenseType.valueOf(code.toUpperCase());
    }
}
