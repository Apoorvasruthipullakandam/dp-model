package com.scholastic.dp.api.model.roster;

import org.hibernate.validator.constraints.Length;

import lombok.Data;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.foundation.core.patch.PatchMerge;
import com.scholastic.foundation.core.patch.PatchMerge.PatchStrategy;

@JsonIgnoreProperties(ignoreUnknown = true)
@PatchMerge(strategy = PatchStrategy.MERGE)
@Data
public class OrgIdentifiers {

    public static final int MAX_LEN = 30;

    @Length(max = MAX_LEN)
    private String ucn;
    
    
}