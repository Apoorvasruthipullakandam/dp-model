package com.scholastic.dp.api.model.subscription;

import org.hibernate.validator.constraints.NotEmpty;

import com.scholastic.foundation.core.patch.PatchMerge;
import com.scholastic.foundation.core.patch.PatchMerge.PatchStrategy;

import lombok.Data;

@Data
@PatchMerge(strategy = PatchStrategy.MERGE)
public class Assignment {

    @NotEmpty
    private String schoolId;

}
