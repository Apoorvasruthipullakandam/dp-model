package com.scholastic.dp.api.model.validation.constraint;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.*;

import javax.validation.Constraint;
import javax.validation.Payload;

import cz.jirutka.validator.collection.CommonEachValidator;
import cz.jirutka.validator.collection.constraints.EachConstraint;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({METHOD, FIELD, ANNOTATION_TYPE})
// this is important!
@EachConstraint(validateAs = EnumValidator.class)
@Constraint(validatedBy = CommonEachValidator.class)
public @interface EachEnum {
    
    Class<? extends Enum<?>> enumType();
    boolean required() default false;
    boolean caseSentitive() default true;
    
    String message() default "invalid.value";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    
}