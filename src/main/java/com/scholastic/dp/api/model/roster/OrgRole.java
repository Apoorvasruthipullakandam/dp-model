package com.scholastic.dp.api.model.roster;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum OrgRole{
    TEACHER("teacher"), STUDENT("student"), SCHOOLADMIN("schoolAdmin");
    
    private String code;
    
    public static OrgRole getOrgRole(String code) {
        return valueOf(code.toUpperCase());
    }
}
