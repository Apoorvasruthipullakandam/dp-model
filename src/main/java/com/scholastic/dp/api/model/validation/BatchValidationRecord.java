package com.scholastic.dp.api.model.validation;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown=true)
@Data
public class BatchValidationRecord  {
	
    private int index;
	    
    private List<SimpleFieldError> fieldErrors;
    private List<RecordError> recordErrors;   
}
