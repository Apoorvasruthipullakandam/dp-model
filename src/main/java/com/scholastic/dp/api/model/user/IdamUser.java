package com.scholastic.dp.api.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scholastic.foundation.core.patch.PatchMerge;
import com.scholastic.foundation.core.patch.PatchMerge.PatchStrategy;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
@PatchMerge(strategy=PatchStrategy.MERGE)
public class IdamUser {
	private String id;
	private String realm;
	private BasicProfile basicProfile;

}
