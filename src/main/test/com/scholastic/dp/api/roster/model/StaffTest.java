package com.scholastic.dp.api.roster.model;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import com.scholastic.dp.api.model.roster.Staff;
import com.scholastic.dp.api.model.user.UserIdentifiers;

public class StaffTest {

    private Staff staff;

    @Before
    public void setUp() throws Exception {
        staff = new Staff();
    }

    @Test
    public void shouldUserIdentifiersNotBeNullWhenRequested() throws Exception {
        UserIdentifiers userIdentifiers = staff.getIdentifiers();
        assertNotNull("UserIdentifiers is null", userIdentifiers);
    }

}
