package com.scholastic.dp.api.roster.model;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.scholastic.dp.api.model.user.UserContext;
import com.scholastic.dp.api.model.user.UserIdentifiers;

public class UserContextTest {

    private UserContext userContext;

    @Mock
    private UserIdentifiers userIdentifiers;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);

        userContext = new UserContext();
        userContext.setUserIdentifiers(userIdentifiers);
    }

    @Test
    public void shouldReturnUserIdentifiersWhenRequested() throws Exception {
        UserIdentifiers someUserIdentifiers = userContext.getUserIdentifiers();
        assertNotNull("userIdentifiers is null", someUserIdentifiers);
    }

    @Test
    public void shouldUserIdentifiersGetIamUserIdWhenRequested() throws Exception {
        when(userIdentifiers.getIamUserId()).thenReturn("1");
        UserIdentifiers someUserIdentifiers = userContext.getUserIdentifiers();
        String iamUserId = someUserIdentifiers.getIamUserId();
        assertNotNull("iamUserId is null", iamUserId);
    }

    @Test
    public void shouldUserIdentifiersGetStaffIdWhenRequested() throws Exception {
        when(userIdentifiers.getStaffId()).thenReturn("1");
        UserIdentifiers someUserIdentifiers = userContext.getUserIdentifiers();
        String staffId = someUserIdentifiers.getStaffId();
        assertNotNull("staffId is null", staffId);
    }

}
