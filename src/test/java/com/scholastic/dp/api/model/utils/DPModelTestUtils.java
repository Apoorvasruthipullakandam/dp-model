package com.scholastic.dp.api.model.utils;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public class DPModelTestUtils {
    
    public static <T> void verifyValid(T bean) {
        Set<ConstraintViolation<T>> constraintViolations = validate( bean );     
        assertEquals("Should have no errors: " + constraintViolations, 0, constraintViolations.size() );     
    }

    
    public static <T> Set<ConstraintViolation<T>> validate(T bean) {
        return VALIDATOR.validate(bean);
    }
    
    public static <T> void verifySinglePatternError(T bean, String propertyPath) {
        verifySingleFieldError(bean, propertyPath, "{javax.validation.constraints.Pattern.message}");
    }
    
    public static <T> void verifySingleFieldError(T bean, String propertyPath, String messageTemplate) {
        Set<ConstraintViolation<T>> constraintViolations = validate( bean );
        
        assertEquals( 1, constraintViolations.size() );
        ConstraintViolation<T> violation = constraintViolations.iterator().next();        
        assertEquals("propertyPath", propertyPath, violation.getPropertyPath().toString() );                
        assertEquals("messageTemplate", messageTemplate, violation.getMessageTemplate() );
    }

    private static Validator createValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        return factory.getValidator();
    }
    private static final Validator VALIDATOR = createValidator();    
}
