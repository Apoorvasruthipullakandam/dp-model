package com.scholastic.dp.api.model.roster;

import static com.scholastic.dp.api.model.utils.DPModelTestUtils.verifyValid;
import static com.scholastic.dp.api.model.utils.DPModelTestUtils.verifySingleFieldError;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.scholastic.dp.api.model.validation.constraint.EnumValidator;

import lombok.AllArgsConstructor;

public class GradeTest {

    @Test
    public void givenParamIsValidCodeWhenGetGradeThenReturnGrade() {
        for(Grade grade : Grade.class.getEnumConstants() ) {
            assertEquals(Grade.getGrade(grade.getCode()), grade);
        }

        // Do one manually for good measure 
        assertEquals(Grade.getGrade("second"), Grade.SECOND);
    }
    
    @Test
    public void givenParamIsValidLabelWhenGetGradeThenReturnGrade() {
        for(Grade grade : Grade.class.getEnumConstants() ) {
            assertEquals(Grade.getGrade(grade.getLabel()), grade);
        }

        // Do one manually for good measure 
        assertEquals(Grade.getGrade("2nd Grade"), Grade.SECOND);
    }
    
    @Test
    public void givenParamIsValidNumericValueWhenGetGradeThenReturnGrade() {
        for(Grade grade : Grade.class.getEnumConstants() ) {
            assertEquals(Grade.getGrade("" + grade.getNumericValue()), grade);
        }

        // Do one manually for good measure 
        assertEquals(Grade.getGrade("2"), Grade.SECOND);
    }
    
    @Test
    public void givenParamIsDifferentCaseLabelWhenGetGradeThenReturnGrade() {
        assertEquals(Grade.getGrade("2nD GrAdE"), Grade.SECOND);
    }
    
    @Test
    public void givenParamIsDifferentCaseCodeWhenGetGradeThenReturnGrade() {
        assertEquals(Grade.getGrade("sEcOnD"), Grade.SECOND);
    }
    
    @Test
    public void givenParamHasExtraWhitespaceWhenGetGradeThenReturnGrade() {
        assertEquals(Grade.getGrade("  2nD GrAdE  "), Grade.SECOND);        
    }
    
    @Test
    public void givenInvalidGradeThenError() {
        GradeBean bean = new GradeBean("xyz");
        verifySingleFieldError(bean, "grade", "error.invalid.grade");
    }
    
    @Test 
    public void givenValidGradeThenOK() {
        verifyValid(new GradeBean("second"));
    }
    
    @Test
    public void givenNullGradeThenOK() {
        verifyValid(new GradeBean(null));        
    }
    
    @AllArgsConstructor
    static class GradeBean {
        @EnumValidator(enumType=Grade.class, message="error.invalid.grade", caseSentitive=false)
        String grade;
    }    
}
