package com.scholastic.dp.api.model.roster;

import static com.scholastic.dp.api.model.utils.DPModelTestUtils.verifySingleFieldError;
import static com.scholastic.dp.api.model.utils.DPModelTestUtils.verifySinglePatternError;
import static com.scholastic.dp.api.model.utils.DPModelTestUtils.verifyValid;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class StudentTest {
	@Test
	public void whenValidStudentThenNoErrors() {
		Student student = validStudent();
		verifyValid(student);
	}

	@Test
	public void whenDoubleSpaceInFirstNameThenError() {
		Student student = validStudent();
		student.setFirstName("Mary  Sue");
		verifySinglePatternError(student, "firstName");
	}

	@Test
	public void whenTrailingSpaceInFirstNameThenError() {
		Student student = validStudent();
		student.setFirstName("Mary Sue ");
		verifySinglePatternError(student, "firstName");
	}
	
	@Test
	public void whenDoubleDashInLastNameThenError() {
		Student student = validStudent();
		student.setLastName("Doe--Dean");
		verifySinglePatternError(student, "lastName");
	}
	
	@Test
	public void whenFirstNameHasNumberThenError() {
		Student student = validStudent();
		student.setFirstName("Ma3y");
		verifySinglePatternError(student, "firstName");
	}	
	
	@Test
	public void whenTrailingSpaceInLastNameThenError() {
		Student student = validStudent();
		student.setLastName("Doe ");
		verifySinglePatternError(student, "lastName");
	}
	
	@Test
	public void whenNonCanonicalGradeThenMakeCanonical() {
	    Student student = validStudent();
	    student.setGrade("  3rD gRaDe  ");
	    assertEquals("Grade should be in canonical form","third", student.getGrade());
	}
	
    @Test
    public void whenInvalidGradeThenBeanValidationError() {
        String invalidGrade = "q";
        Student student = validStudent();
        student.setGrade(invalidGrade);
        assertEquals("grade", invalidGrade, student.getGrade()); // We should store the invalid grade
        verifySingleFieldError(student, "grade", "error.invalid.grade");
    }

	private Student validStudent() {
		Student student = new Student();
		student.setFirstName("John");
		student.setLastName("Doe");
		student.setGrade("first");			
		return student;
	}
}
