package com.scholastic.dp.api.model.validation;

import static com.scholastic.dp.api.model.utils.DPModelTestUtils.verifySingleFieldError;
import static com.scholastic.dp.api.model.utils.DPModelTestUtils.verifyValid;

import org.junit.Test;

import com.scholastic.dp.api.model.roster.Grade;
import com.scholastic.dp.api.model.validation.constraint.EnumValidator;

import lombok.RequiredArgsConstructor;

public class EnumValidatorImplTest {

    @Test
    public void whenOptionalThenAllowNulls() {        
           verifyValid(new GradeOptional(null));        
    }
    
    @Test 
    public void whenRequiredThenRejectNulls() {
        verifySingleFieldError(new GradeRequired(null), "grade", "test.message");
    }
}

@RequiredArgsConstructor
class GradeRequired{
    @EnumValidator(enumType=Grade.class, message="test.message", required=true)
    final String grade;            
}

@RequiredArgsConstructor
class GradeOptional{
    @EnumValidator(enumType=Grade.class, message="test.message")
    final String grade;            
}


